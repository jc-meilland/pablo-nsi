---
author: Meilland
title: Representation binaire des entiers
tags:
  - 2-Representation binaire des entiers
---

La fonction `addition` prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux.

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/addition') }}
