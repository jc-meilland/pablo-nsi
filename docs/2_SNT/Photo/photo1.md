---
author: Meilland jean claude
title: 2° SNT - Photos partie 1
---



## Le capteur CCD

###  Principe de fonctionnement d'un appareil photo numérique


Le capteur ***CCD*** (**charge coupled device**) d'un appareil photo numérique est une mosaïque constituée par l'assemblage sous la forme d'une matrice, d'éléments sensibles à la lumière. Chacun de ces éléments est appelé photosite (la taille d'un photosite est de l'ordre de 5 µm).

Un photosite accumule une charge électrique proportionnelle à la quantité de lumière qu'il reçoit.

<center>

![photosite](./images/filtre_bayer.svg){ width=200px }
![](./images/filtre_bayer2.svg){ width=100px }

</center>


Pour avoir une image couleur il faut "spécialiser" chaque photosite pour une couleur primaire.

Pour cela on utilise un filtre (appelé filtre de Bayer) constitué d'un quadruplet de filtres colorés (Vert, Rouge, Bleu, Vert) placé sur un quadruplet de photosites. Chaque quadruplet est composé de 2 éléments verts pour seulement 1 rouge et 1 bleu, pour venir calquer l'anatomie de l'œil
humain.

- si un quadruplet est éclairé par de la lumière blanche, les quatres photosites accumuleront une charge identique et le calculateur en déduira qu'il s'agit du blanc.
- si le quadruplet est éclairé par de la lumière rouge, seul le filtre rouge laisse passer la lumière et donc seul le photosite rouge aura accumulé une charge...

### REMARQUE

Pour qu'un smartphone délivre une photo d'une définition de 4000 × 3000 pixels soit 12 mégapixels, il faut un
capteur avec 4 × 12 = 48 millions de photosites ! ! !

### EXERCICE

Chaque composante rouge, vert, bleu est codée sur un octet, c'est à dire sur 256 niveaux d'intensité (allant de 0 à 255).  
Ainsi, chaque pixel pèse 3 octets.


1. Une image a une définition de 4096 × 3072 pixels. Quelle est sa taille en octets si on l'enregistre sans la compresser ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
2. Pour envoyer la photo par MMS, on la réduit à la définition de 1024 × 768. Combien pèse-t-elle désormais ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
3. Une imprimante a une résolution de 300 ppp (dpi en anglais), soit 300 pixels par pouce (un pouce = 2,54 cm).

  1. Pour cette imprimante, quelle est la définition d'une image carrée de 25,4 cm de côté ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  2. On désire imprimer notre photo au format 12 cm × 9 cm. Est-ce que la définition de 1024 × 768 est sufﬁsante ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


### IMPORTANT : NE PAS CONFONDRE DÉFINITION ET RÉSOLUTION

La définition est le produit de la largeur par la hauteur en pixels. Elle se mesure en . . . . . . . . . . . . . . . . . . . . . . . . . . .

La résolution est le nombre de pixels par unité de longueur. Elle se mesure en . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


1

## LE CODAGE RVB

### PRINCIPE

Comme on l'a vu lors de la partie précédente, le codage RVB consite à coder chaque couleur par addition des trois couleurs élémentaires : Rouge, Vert, Bleu.  
Ainsi, on peut mémoriser un pixel RVB en mémorisant trois nombres entiers compris entre 0 et 255, un pour chaque couleur. On dit que le codage a lieu sur 3 octets.

### EXERCICE : COMPLÉTER LE TABLEAU CI DESSOUS ET RÉPONDRE AUX QUESTIONS

1. Combien peut-on générer de couleurs différentes avec le codage RVB ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
2. Combien peut-on générer de nuances de gris différentes avec le codage RVB ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
3. Combien peut-on générer de nuances de rouge différentes avec le codage RVB ?  
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
4. Ci dessous sont représentés les disques de la synthèse additive des couleurs.

| Couleur | R  | V | B | 
| :---: | :---: | :---: | :---: |
| Noir   |  0  | 0 | 0  |
| Blanc | 255 | 255 | 255 |
| Rouge | 255 | 0 | 0 |
| Rouge clair | 255 | 150 | 150 |
| Rouge très clair | 255 | 200 | 200 |
| Bleu   |  |  |  |
| Bleu clair  |  |  |  |
| Vert  |  |  |  |
| Vert clair  |  |  |  |
| Gris  |  |  |  |
| Gris clair  |  |  |  |
| Jaune  |  |  |  |
| Magenta  |  |  |  |
| Cyan  |  |  |  |


2

a) Comment peut-on coder un jaune ?

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

b) Comment peut-on coder un magenta ?

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

c) Comment peut-on coder un cyan ?

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

PARTIE 3:
:

CRÉATION D'UN PROGRAMME PERMETTANT DE CRÉER UNE IMAGE PIXEL PAR PIXEL

>

1.

PRINCIPE

En Python, la bibliothèque PIL permet de créer une image et de manipuler ses pixels

E

2. EXEMPLE

Le programme ci-dessous déﬁnit une image de définition 2 × 4 pixels, pixel par pixel puis la sauvegarde sous le nom
image.png

from PIL import Image

monimage = Image.new('RGB', (2,4))

monimage.putpixel((0, 0), (255, 0, 0))
monimage.putpixel((0, 1), (255, 0, 0))
monimage.putpixel((0, 2), (255, 0, 0))
monimage.putpixel((0, 3), (255, 0, 0))
monimage.putpixel((1, 0), (0, 255, 0))
monimage.putpixel((1, 1), (0, 255, 0))
monimage.putpixel((1, 2), (0, 0, 255))
monimage.putpixel((1, 3), (0, 0, 255))

monimage.save("image.png")

En s'inspirant du programme précédent, créer un programme appelé drapeau.py permettant
d'obtenir une image de définition 3 × 3 pixels comme ci-contre appelée drapeau.png

DEPÔT 1

drapeau.py et drapeau.png sur http://entraide-ella.fr

mp

3. MINI-PROJET : PIXEL-ART

1. Concevoir sur la grille ci-contre une image de définition 5 × 5 de son choix.
2. Créer un programme nommé imageperso.py permettant de fabriquer cette image

et de la sauver sous le nom imageperso.png

DEPÔT 2

imageperso.py et imageperso.png sur http://entraide-ella.fr

© S. COLOMBAN – 2021/2022 – Lycée Ella Fitzgerald

3

PARTIE 4:
:

CRÉATION D'UN PROGRAMME PERMETTANT D'ISOLER LES COMPOSANTES RVB

E

1. EXERCICE

Pour obtenir la composante rouge d'une photo, il sufﬁt de parcourir chacun de ses pixels aﬁn d'obtenir sa couleur
(r,v,b) puis de créer une photo où le pixel correspondant est (r,0,0)

1. La fonction suivante crée une image de la composante rouge d'une photo :

(largeur, hauteur)= img.size
image=Image.new('RGB', (largeur,hauteur))
for x in range(largeur):

for y in range(hauteur):

1 def composanterouge(img):
2
3
4
5
6
7
8
9
10
11

return image

(rouge,vert,bleu) = img.getpixel((x,y))
vert = 0
bleu = 0
image.putpixel((x,y),(rouge,vert,bleu))

a) modiﬁer les lignes 1, 7 et 8 aﬁn de créer une fonction composanteverte

1 ...

7 ...

8 ...

b) modiﬁer de même les lignes 1, 7 et 8 aﬁn de créer une fonction composantebleue

1 ...

7 ...

8 ...

2. Récupérer le programme S1_photonum.py et la photo S1_joconde.png

3. Ouvrir Thonny puis éditer le programme S1_photonum.py aﬁn compléter les fonctions composanteverte et composantebleue.

4. Lancer votre programme.

DEPÔT 3


Référence:  © S. COLOMBAN – 2021/2022 – Lycée Ella Fitzgerald
